
import sys
if len (sys.argv) < 3:
    print ("Insufficiently arguments! look: >> script.py ip user <<")
    raise SystemExit


import os.path
import shutil 

ip = format (sys.argv[1])
user = format (sys.argv[2])
XML_FILE_SRC = "/var/lib/jenkins/myapplication/jen/jobs/mainjob/config.xml"
XML_FILE_DEST = "/var/lib/jenkins/jobs/mainjob/config.xml"
pathf = "/var/lib/jenkins/myapplication/ans/keys/key_rsa_"+ip

shutil.copyfile(XML_FILE_DEST, XML_FILE_SRC)   

import xml.etree.ElementTree as ET
tree = ET.parse(XML_FILE_SRC)
root = tree.getroot()
a = root[3][0][0][4][2][0]
kstr = ET.Element('string')
kstr.text = pathf
a.append(kstr)
dvns = root[3][0][0][0][2]
dvns.text = ip
dvns = root[3][0][0][5][2]
dvns.text = user
tree.write(XML_FILE_SRC)

shutil.copyfile(XML_FILE_SRC, XML_FILE_DEST)   

from subprocess import call
if os.path.exists(pathf):
	call(["rm", pathf])
if os.path.exists(pathf+".pub"):	
	call(["rm", pathf+".pub"])
call(["cp", XML_FILE_SRC, XML_FILE_DEST])
call('ssh-keygen -t rsa -N "" -f '+pathf, shell=True) 
cres=call(["ssh-copy-id", "-i", pathf+".pub", user+"@"+ip])
if (cres != 0):
	raise SystemExit	
call('ansible all -i "'+ip+'," -m ping --private-key='+pathf+
	' -u '+user+' -e ansible_python_interpreter=/usr/bin/python2.7', shell=True)
call(["service", "jenkins", "restart"])