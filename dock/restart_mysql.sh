#!/usr/bin/env bash

docker-compose -f docker-compose.yml down
img_id="$(docker images -q mysqldb)"
docker rmi -f $img_id
docker-compose -f docker-compose.yml up -d